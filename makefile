programme_dyn: main.o SDLfunc.o affectations.o affichage.o
	gcc -Wall main.o SDLfunc.o affichage.o affectations.o -o programme_dyn -lglut -lm -lGLU -lGL  `sdl-config --cflags --libs` -lSDL_image -lpthread -lX11
	
main.o: main.c planete.h

affectations.o: affectations.c planete.h
	gcc -c -Wall affectations.c

affichage.o: affichage.c planete.h
	gcc -c -Wall affichage.c

SDLfunc.o: SDLfunc.c planete.h
	gcc -c -Wall SDLfunc.c

clean:
	rm -f *~ *.o

