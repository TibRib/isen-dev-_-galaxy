#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include <dirent.h> 
#include <time.h>
#include <string.h>

#include "planete.h"

//Threads

//Donne 2 million d'annees par seconde à la planete.
//Releve la vitesse à l'instant t pour comparaison future.
void *age(void * arg){
	while(1){
			sleep(1);
			if(menu ==4){
				if(Terre.stadeDeVie < 4){
				Terre.age += 2;
				}
				else Terre.age +=0.1;
			
				if(Terre.stadeDeVie ==7)
					nbsat++;
					
				prevVel = Terre.velocite;
				}
		}
}

//Fait retrecir la variable hauteur employée par l'affichage de la camera, toutes les 1/2s.
//But: créer une animation au lancement du programme.
void *launchIntro(void * arg){
	while(1){
			usleep(50000);
			hauteur -= hauteur*0.06;
			
			//printf("H: %.2f\n",hauteur);
			
			if(hauteur < 1 && intro == 1 ){
				intro = 0;
			}
			if(hauteur < 0.01){
				//printf("\n Thread d'intro killed \n");
				pthread_exit(0);
			}
		}

}

//Gère les états et l'avancée de la vie
void *gere_vivant(void * arg){
	int chance;
	while(1){
		if(menu ==4 && intro ==-1 && Terre.age >2){
			//On verifie si les conditions se pretent a la vie (humidite > 50% et temperature entre 15 et 30°C)
			if(Terre.humidite > 50 && Terre.temperature > 270 && Terre.temperature < (30+273)){
				//On genere un entier aleatoire
				//On a etabli le pourcentage de probabilité de l'apparition d'une forme de vie en entete (.h)
				
				
				if(Terre.stadeDeVie < 1)
					chance = aleatoireBorne(1,100); //PROBA_VIE% de chance si la vie n'est jamais apparue;
					
				else if(Terre.stadeDeVie < 7) chance = aleatoireBorne(1,70); // PROBA_VIE%+20% si vie deja apparue
				
				if(Terre.humidite > 85 && Terre.stadeDeVie == 3){ printf("La vie Sous-marine continue car trop d'humidite...\n"); sleep(10);}
				else{
						printf("\033[1mTemperature moyenne :\033[33m %d°C \033[0m\n",Terre.temperature-273);
						if(Terre.stadeDeVie < 1) printf("\nLa vie est possible, voyons si elle se developpe...\n");
						else if(Terre.stadeDeVie < 7) printf("\nLa vie est susceptible de subir une evolution, voyons si elle se developpe...\n");
									
						if(chance <= PROBA_VIE && Terre.stadeDeVie < 7){
								Terre.stadeDeVie += 1;
								printf("\033[1mLa vie sur votre planete a franchi un nouveau cap\033[0m ! Felicitation !\n");
								sleep(20);	//20 secondes d'intervalle
								
							}
						else if(chance >= 50 && Terre.stadeDeVie > 1){
							printf("\033[1mOh mince ! la vie vient de regresser !\033[0m\n");
							Terre.stadeDeVie -= 1;
							nbsat =0;
						}
						else{
							if(Terre.stadeDeVie < 7) printf("La vie ne pointe pas encore le bout de son nez...  Patientez encore\n");
							//Sinon, on attend encore de nombreuses années que la planete retente d'accueillir de la vie
							sleep(10);	//10 secondes d'intervalle
						}
						
					}
			}
			else{
				if(Terre.stadeDeVie > 0){ printf("\033[31;1;4;7m  Toute trace de vie vient de disparaitre \033[0m \n"); Terre.stadeDeVie = 0; nbsat=0;}
				printf("\nvie impossible:\n");
				if(Terre.temperature > (30+273)) printf("\033[34;1m Baisser la temperature...\033[0m\n");
				if(Terre.temperature < 270 ) printf("\033[31;1m Monter la temperature...\033[0m\n");
				if(Terre.humidite < 51 && Terre.temperature > 270 && Terre.temperature < 303) printf("Rappel : \033[36;1ml'eau est une condition necessaire de la vie : Faites grimper l'humidite\033[0m\n");
				printf("\033[1mTemperature moyenne :\033[33m %d°C \033[0m\n",Terre.temperature-273);
				sleep(5); //Le thread patiente pendant 5 secondes
			}
		}
	}

}

planete initPlanete(char* chaine_nom){
	planete p;
	
	strcpy(p.name, chaine_nom);
	p.age = 1;
	p.masse = 25;
	p.temperature = 0;
	p.velocite = 2200;
	p.humidite = 0;
	p.stadeDeVie = 0;
	p.atmosphere = false;
	p.stadeDePlanete = 1;
	p.hollow =0;

	
	return p;
}

void updateGeneration(int gen){
	char nom_fichier[40];
	char buff[2];
	char format[4];
	if(Terre.stadeDePlanete <= 5)
	{
		Terre.stadeDePlanete = gen;
			
		Terre.generation = aleatoireBorne(1,4);
		
		sprintf(buff,"%d",Terre.generation);
		
		
		strcpy(nom_fichier,"img/");
		
		if(Terre.stadeDePlanete == 1) //Etat de pierre
			strcat(nom_fichier,"pierre/");
			
		if(Terre.stadeDePlanete == 2) //Etat de fusion liquide
			strcat(nom_fichier,"magma/fusion/");
			
		if(Terre.stadeDePlanete == 3) //Etat post fusion
			strcat(nom_fichier,"magma/solide/");
			
		if(Terre.stadeDePlanete == 4) //Etat de croissance
			strcat(nom_fichier,"autre/");
			
		if(Terre.stadeDePlanete >= 5) //Etat de vie
			strcat(nom_fichier,"life/");
		
		if((aleatoireBorne(0,2) == 1 && Terre.stadeDePlanete != 2) || Terre.stadeDePlanete == 5){ 
			strcpy(format,"png"); 
		}
		else { 
			strcpy(format,"jpg"); 
		}
		
		strcat(nom_fichier,format);
		strcat(nom_fichier,"/");
		strcat(nom_fichier,buff);
		strcat(nom_fichier,".");
		strcat(nom_fichier,format);
		
		
		printf("chargement de la texture '%s'.\n",nom_fichier);
		
		texture_terre = loadTexture(nom_fichier,false);
	}
}

void gagnemasse(void){
	
	if(xaa < 5 && xaa != -101){
		float calc = (1 - exp(-0.00125*(Terre.velocite)));	//Prise de moins en moins consequente au fil de la rotation
		//float calc = Terre.velocite/1000;
		Terre.masse += calc/100;
	}
	
	if(Terre.velocite <1) Terre.velocite = 1;
	if(Terre.velocite - log(Terre.velocite)/3000 > 1)
		Terre.velocite -= log(Terre.velocite)/3000;	//Baisse de la vitesse avec augmentation de celle ci


}

int croissance(float a, float b){
	//Si l'on considere deux termes d'une suite,
	//La valeur a est la valeur Un, tandis que la valeur b est la valeur Un+1.
	
	if(a < b)
		return 1; //Croissance positive
	else if(a > b)
		return -1;	//Croissance negative
	
	return 0; //Croissance nulle
	
}

void degazage(void){
	//Calcul du degazage : phenomene de generation de l'humidité apparent lorsque la planete perd +/- drastiquement de vitesse
	if(Terre.stadeDePlanete >= 3){
		if(Terre.velocite+5.0 < prevVel){
			Terre.humidite += (float) (prevVel - Terre.velocite)/50.0;
			
			prevVel = Terre.velocite;
		}
	}
	else{
		Terre.humidite = 0;
	}
	
}

void temperature(void){
	//Calcul de Temperature en fonction de la vitesse de rotation
		//Coefficient de proportionnalité établi a partir de la planète Terre (15°C pour 1142 km/s)
	Terre.temperature = 0.252*Terre.velocite;
}

int aleatoireBorne(int a, int b){
	//Genere un entier aleatoire défini sur [a,b[
	return ((rand()%(b-a)) + a);
}

int sauvePlanete(planete p){
	
	char destination[80]; 
	
	// A allouer dynamiquement une fois la longueur de la chaine p.nom connue.
	
	//char* destination;
	//destination = (char*) malloc(sizeof(char) * (strlen(p.nom) + strlen("save/ .planet")) );
	
	strcpy(destination, "save/");
	strcat(destination, p.name);
	strcat(destination,".planet");
	
	FILE * f;
	f = fopen(destination,"wb");
	
	if(f == NULL)
		return 1;
	
	fwrite(&p, sizeof(planete),1,f);
	
	fclose(f);
	return 0;
}

int listeFichiersDirectory(char * dossier, char*** liste, char* extension)
{
	//On cherche à lister tous les fichiers du repertoire (dossier) concerné.
	int nb =0;
	DIR *d;
	struct dirent *dir;
	d=opendir(dossier);
	
	char c;
	int l_ex =0;
	char cur_ex[50];
	int a;
	
	//Si le dossier existe,
	if(d){
		//Tant qu'il existe des fichiers dans ce repertoire,
		while  ((dir = readdir(d)) != NULL){
			l_ex =0;
			//Enregistrement de l'extension
			for(int pt=0, a=0; a <= strlen(dir->d_name); a++){
				c = dir->d_name[a];
				if( c == '.') pt = 1;
				if(pt == 1){
					cur_ex[l_ex] = c;
					l_ex++;
				}
			}
			cur_ex[l_ex+1] = '\0';
			
			//On verifie que l'extension enregistree = extension demandee
			if( strcmp(extension, cur_ex) == 0){
				//C'est bon, on incremente le nombre de fichiers reconnus
				nb++;
				//Puis on enregistre le nom du fichier sans extension dans le tableau
				for(a=0; a < strlen(dir->d_name)-strlen(extension); a++){
					(*liste)[nb-1][a] = dir->d_name[a];
				}
				(*liste)[nb-1][a] = '\0';
			}
		}
		closedir(d);
		//Dossier fermé.
	}
	return nb;
}

void chargePlanete(planete* p){
	
	planete* maSave;
	maSave = malloc(sizeof(*p));
	
	// Allocation dynamique
	
	char* destination  = (char*) malloc( sizeof(char) * strlen(p->name) + strlen("save/ .planet ")  );
	
	strcpy(destination, "save/");
	strcat(destination, (*p).name);
	strcat(destination,".planet");
	
	FILE * f;
	f = fopen(destination,"rb");
	
	if(f == NULL){
		printf("\033[31mImpossible de charger la sauvegarde\033[0m\n");
		quitBtn();
	}
	else{
		fread(maSave, sizeof(planete), 1, f);
	}
	
	fclose(f);
	free(destination);
	
	*p = *maSave;
	
	free(maSave);
	
	updateGeneration(p->stadeDePlanete);
}

//APPELS DES BOUTONS
void showNoyau(void)
{
	printf("\033[34mFonction d'affichage noyau();\033[0m\n");
	if(Terre.hollow == 1) Terre.hollow =0;
	else Terre.hollow = 1;
}

void showLights(void)
{
	if(eclairage == 1){
		 eclairage = 0;
		 EclairageBtn.label = "Eclairage : Inactif";
		 printf("\033[34mEclairage desactive\033[0m\n");
	 }
	else{
		 eclairage = 1;
		 EclairageBtn.label = "Eclairage : Actif";
		 printf("\033[34mEclairage actif\033[0m\n");
	 }
}

void startBtn(void){
	if(menu == 1 && strlen(Terre.name) <1){
		strcpy(Terre.name, "no_name");
		}
	if(menu == 1)
		menu = 3;
	else if(menu == 0){
		menu = 1;
		
		ChargerBtn.label = "Valider";
		ChargerBtn.callbackFunction = startBtn;
	}
}

void chargeBtn(void){
	if(menu == 2 && strlen(Terre.name) >1){
		chargePlanete(&Terre);
		menu = 3;
	}
	if(menu == 0){
		nbPlanetesSaved = listeFichiersDirectory("save", &liste, ".planet");
		
		 menu = 2;
		ChargerBtn.state = 0;
		ChargerBtn.label = "Charger";
		ChargerBtn.callbackFunction = chargeBtn;

	 }
}

void saveBtn(void){
	sauvePlanete(Terre);
	printf(" ####\033[45m Planete %s Sauvegardee ! \033[0m#### \n",Terre.name);
	menu = 4;
	sauverBtn.state =0;
}

void quitBtn(void){
	exit(0);
}

int mouseIn(mouse m, float x1, float y1, float x2, float y2){
	//glColor3f(0.8f,0.0f,0.0f);
	//rectangleCreux( x1, y1, x2, y2); //affichage zone de clic (dev)
	if(m.passif_x > x1 && m.passif_x < x2 && m.passif_y > y1 && m.passif_y < y2){
		return 1;
	}
	return 0;
}
