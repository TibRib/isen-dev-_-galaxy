#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <stdbool.h>
#include <math.h>
#include <dirent.h> 
#include <string.h>

#include "planete.h"

#define PI 3.14159

void drawCercle(float r, float x, float y, int segments)
{
    glBegin(GL_LINE_LOOP);
    for(int i = 0; i < segments; i++)
    {
        float theta = (2*PI*i)/ segments;

        float vx = r * cos(theta);
        float vy = r * sin(theta);

        glVertex2f(x + vx, y + vy);

    }
    glEnd();
}

void drawDisque(float r, float x, float y, int segments)
{
    glBegin(GL_POLYGON);
    for(int i = 0; i < segments; i++)
    {
        float theta = (2*PI*i)/ segments;

        float vx = r * cos(theta);
        float vy = r * sin(theta);

        glVertex2f(x + vx, y + vy);

    }
    glEnd();
}




/* Affiche une chaine de caracteres de taille donnee a la position specifiee */
void afficheChaine(const char *chaine, float taille, float x, float y)
{
	
	glPushMatrix();
	  glLoadIdentity();
	  
	char *pointeurChaine = (char *)chaine;
	const float tailleScaling = taille/120.f;
	glScalef(tailleScaling, tailleScaling, tailleScaling);
	glTranslatef(x,y,0);
	
	  	while (*pointeurChaine){
		glutStrokeCharacter(GLUT_STROKE_ROMAN, *pointeurChaine++);
	}
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	
		

}

/* Renvoie l'espace occupe par la chaine de caracteres de taille donnee */
float tailleChaine(const char *chaine, float taille)
{
	return glutStrokeLength(GLUT_STROKE_ROMAN, (const unsigned char *)chaine)*taille/120.f;
}

void rectanglePlein(float x1, float y1, float x2, float y2){
	glBegin(GL_QUADS);
		glVertex2f(x1, y1);
		glVertex2f(x2, y1);
		glVertex2f(x2, y2);
		glVertex2f(x1, y2);
	glEnd();
}

void rectangleCreux(float x1, float y1, float x2, float y2){
	glBegin(GL_LINE_STRIP);
			glVertex2f(x1, y1);
			glVertex2f(x1, y2);
			glVertex2f(x2, y2);
			glVertex2f(x2, y1);
			glVertex2f(x1, y1);
		glEnd();
}

/*Fais la transition entre ancienne et nouvelle texture */
void GradTextures(GLuint oldTex, GLuint newTex, int secondes){
	/*Cette fonction n'a pour l'instant aucune utilité : prevision future: mixer les textures lors de l'evolution de la planete */
	//if(fading == 1){
		
			glBindTexture(GL_TEXTURE_2D, newTex);
		
	//}
	//else{
		//secFade = (float)Terre.age;
		//fading = 1;
	//}

}

void EndGrad(void){
	glDisable(GL_BLEND);
}

void afficheStats(planete p){
	
	  glMatrixMode(GL_PROJECTION);
	  glPushMatrix();
	  glLoadIdentity();
	  gluOrtho2D(0.0, lF, 0.0,hF);
	  glMatrixMode(GL_MODELVIEW);

	  

	char chaine[100];
	char buffer[25];
	
	glRasterPos2i(10, 10);
	glColor3f(0.8, 0.8, 0.8);
	
	//Affichage de 8 parametres
	for(int i = 14; i >=1 ; i-=2){
		strcpy(chaine,"");
		strcpy(buffer,"");
		
		if(i == 14){		strcpy(chaine,"Appelation : "); strcat(buffer, p.name); }
		if(i == 12){	strcpy(chaine,"Age : ");	sprintf(buffer,"%.1f",p.age); 
							if(p.age > 1)	strcat(buffer," millions d'annees");  else strcat(buffer," million d'annees");  }
		if(i == 10){	strcpy(chaine,"Masse : ");	sprintf(buffer,"%.1f",p.masse);  strcat(buffer," x10^22 kg"); }
		if(i == 8){		strcpy(chaine,"Temperature : ");	sprintf(buffer,"%d K/ %d C",p.temperature,p.temperature-273); }
		if(i == 6){		strcpy(chaine,"Humidite : ");	sprintf(buffer,"%.2f",p.humidite); strcat(buffer," %"); }
		if(i == 4){		strcpy(chaine,"Vitesse de rotation : ");	sprintf(buffer,"%.0f",p.velocite); strcat(buffer," Km/s");}
		if(i == 2){		strcpy(chaine,"Etat de la vie : ");
							if (p.stadeDeVie == 0) sprintf(buffer,"Inexistante");
							if (p.stadeDeVie == 1) sprintf(buffer,"Procaryotes");
							if (p.stadeDeVie == 2) sprintf(buffer,"Eucaryotes");
							if (p.stadeDeVie == 3) sprintf(buffer,"Sous-marine");
							if (p.stadeDeVie == 4) sprintf(buffer,"Reptiles terrestres");
							if (p.stadeDeVie == 5) sprintf(buffer,"Mammiferes/reptiles");
							if (p.stadeDeVie == 6) sprintf(buffer,"Mammiferes/Hominides");
							if (p.stadeDeVie == 7) sprintf(buffer,"Hominides");
						}
						
		
		strcat(chaine,buffer);
		afficheChaine(chaine,lF/100 + hF/100, lF/6, i*hF/8);
	}
	

	  glMatrixMode(GL_PROJECTION);
	  glPopMatrix();

}

void fondMenu(void)
{
    glPushMatrix();

		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);

		gluQuadricDrawStyle(params,GLU_FILL);	
		
			if(eclairage == 1){
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
					glRotated(angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);	
			glLightiv(GL_LIGHT0,GL_POSITION,LightPos);
					glRotated(-angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);
		}
		
		glColor3f(1.0f,1.0f,1.0f);
		
		glTranslatef(1.25,5.25,93.6+(angle_camera*VITESSE_ROTATION_CAMERA*0.025));
		glRotatef(60,1,1,0); 
		
		
		glBindTexture(GL_TEXTURE_2D,texture_perf);
		//Dessin des spheres
		
		gluSphere(params,2.8,50,50);
		
		
		glBindTexture(GL_TEXTURE_2D,texture_clouds);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		
		glRotated(angle_camera*VITESSE_ROTATION_CAMERA*0.2,0,0,1);
		//Dessin de la sphere : couche nuageuse
		gluSphere(params,2.8+0.025,50,50);
		//Antirotation (+1-1 =0)
		glRotated(-angle_camera*VITESSE_ROTATION_CAMERA*0.2,0,0,1);
		
			
		glDisable(GL_BLEND);
	
		gluDeleteQuadric(params);
		
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
		
		glRotatef(-60,1,1,0);
		

    glEnd();
    glPopMatrix();

}

void drawCube(float x, float y, float z){
	//X designe la largeur sur x;
	//Y designe la longueur sur y;
	//Z designe la position sur z;
	// BACK
	glBegin(GL_POLYGON);
	glVertex3f(  x, -y, z );
	glVertex3f(  x,  y, z );
	glVertex3f( -x,  y, z );
	glVertex3f( -x, -y, z );
	glEnd();
	
	// FRONT
	glBegin(GL_POLYGON);
	glVertex3f(  x, -y, -z );
	glVertex3f(  x,  y, -z );
	glVertex3f( -x,  y, -z );
	glVertex3f( -x, -y, -z );
	glEnd();
	 
	// RIGHT
	glBegin(GL_POLYGON);
	glVertex3f( x, -y, -z );
	glVertex3f( x,  y, -z );
	glVertex3f( x,  y,  z );
	glVertex3f( x, -y,  z );
	glEnd();
	 
	// LEFT
	glBegin(GL_POLYGON);
	glVertex3f( -x, -y,  z );
	glVertex3f( -x,  y,  z );
	glVertex3f( -x,  y, -z );
	glVertex3f( -x, -y, -z );
	glEnd();
	 
	// TOP
	glBegin(GL_POLYGON);
	glVertex3f(  x,  y,  z );
	glVertex3f(  x,  y, -z );
	glVertex3f( -x,  y, -z );
	glVertex3f( -x,  y,  z );
	glEnd();
	 
	//  BOTTOM
	glBegin(GL_POLYGON);
	glVertex3f(  x, -y, -z );
	glVertex3f(  x, -y,  z );
	glVertex3f( -x, -y,  z );
	glVertex3f( -x, -y, -z );
	glEnd();
}

void drawPale(float x, float l, float z){ //POUR SATELLITES
	//RIGHT
	glBegin(GL_POLYGON);
	glColor3f(   0.071,  0.447, 0.792 );
	glVertex3f(  x, -x, z );
	glVertex3f(  x,  x, z );
	glVertex3f(  x+l,  x, z );
	glColor3f(   0.94,  0.97, 0.996 );
	glVertex3f(  x+l, -x, z );
	glEnd();
	
	//LEFT
	glBegin(GL_POLYGON);
		glColor3f(   0.071,  0.447, 0.792 );
	glVertex3f(  -x, -x, z );
	glVertex3f(  -x,  x, z );
	glVertex3f(  -x-l,  x, z );
		glColor3f(   0.94,  0.97, 0.996 );
	glVertex3f(  -x-l, -x, z );
	glEnd();
	
}

void drawSatellite(int nb)
{
	glDisable(GL_TEXTURE_2D);
    glPushMatrix();

	GLUquadric* params = gluNewQuadric();
	gluQuadricTexture(params,GLU_TRUE);
	gluQuadricDrawStyle(params,GLU_FILL);	
		
	float speed = VITESSE_ROTATION_CAMERA+SAT_SPEED, r = rayon + 1, size = 0.015;

	for(int a =0; a< nb; a++){
		glRotated(45*a, 0, 1, 0);
		glRotated(32*a, 1, 0, 0);
		glRotated(angle_camera*speed/r,0,0,1);
		
		glTranslatef(r,0,0);
		
		//Dessin du satellite
		glRotated(rotatex,0,0,1);
		glColor3f(   0.839,  0.639, 0.329 );
		drawCube(size,size,size);
		drawPale(size,4*size,0);
		
		glTranslatef(0,0,size+ size/10);
		glColor3f(   0.6,  0.6, 0.6 );
		drawDisque(size/2,0,0,12);
		
		glTranslatef(0,0,-size/10);
		glColor3f(   0.8,  0.8, 0.8 );
		gluCylinder(params, size/2,size,size, 12, 6);
		glRotated(-rotatex,0,0,1);
		
		glTranslatef(-r,0,0);
		
		glRotated(-angle_camera*speed/r,0,0,1);


	}
	
	
	gluDeleteQuadric(params);
    glEnd();
    glPopMatrix();
    glEnable(GL_TEXTURE_2D);

}

void drawPoussiere(void)
{
    glPushMatrix();

		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);
			
	glMaterialiv(GL_FRONT_AND_BACK,GL_SPECULAR,MatSpec);
	glMateriali(GL_FRONT_AND_BACK,GL_SHININESS,100);

		gluQuadricDrawStyle(params,GLU_FILL);	
		
			if(eclairage == 1){
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
					glRotated(angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);	
			glLightiv(GL_LIGHT0,GL_POSITION,LightPos);
					glRotated(-angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);
		}
		
		float x = 0, y = 0;
		glTranslatef(x,y,0);
		
		glColor3f(1.0f,1.0f,1.0f);
		//drawDisque(0.1,-x,-y,5); 	//Affiche un curseur au centre de la zone de rotation

		float speed = 0.1;
		glBindTexture(GL_TEXTURE_2D,texture_caillous);
		//Dessin des spheres
		
		for(int a =0; a< 8; a++){
			float r = xaa;
			glRotated(45*a, 0, 1, 1);
			
			glRotated(angle_camera*speed/r,0,0,1);
			glTranslatef(r,0,0);
			gluSphere(params,0.1,3,3);
			glTranslatef(-r,0,0);
			glRotated(-angle_camera*speed/r,0,0,1);
			
			glRotated(-45*a, 0, 1, 1);
		}
		
		for(int a =0; a< 6; a++){
			float r = xaa*2;
			glRotated(60*a, 0, 1, 1);
			
			glRotated(angle_camera*speed/r,0,0,1);
			glTranslatef(r,0,0);
			gluSphere(params,0.15,3,3);
			glTranslatef(-r,0,0);
			glRotated(-angle_camera*speed/r,0,0,1);
			
			glRotated(-60*a, 0, 1, 1);
		}
		
		for(int a =0; a< 4; a++){
			float r = xaa*3;
			glRotated(90*a, 0, 1, 1);
			
			glRotated(angle_camera*speed/r,0,0,1);
			glTranslatef(r,0,0);
			gluSphere(params,0.2,3,3);
			glTranslatef(-r,-r/3,0);
			glRotated(-angle_camera*speed/r,0,0,1);
			
			glRotated(-90*a, 0, 1, 1);
		}
	
		gluDeleteQuadric(params);
		
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
		

    glEnd();
    glPopMatrix();

}

void drawSphere(float rayon, int prec)
{
    glPushMatrix();

		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);
			
	glMaterialiv(GL_FRONT_AND_BACK,GL_SPECULAR,MatSpec);
	glMateriali(GL_FRONT_AND_BACK,GL_SHININESS,100);
	
	if(prec < 3) prec = 3;
	if(prec > 50) prec = 50;
	
	//Noyau terrestre
		gluQuadricDrawStyle(params,GLU_FILL);
		
		glBindTexture(GL_TEXTURE_2D,texture_noyau);
			//Dessin de la sphere : noyau interne
			gluSphere(params,rayon*17/100,prec,prec);
			
	//Couche exterieure
	
		//Eclairage
		if(eclairage == 1 && (Terre.stadeDePlanete >3 || Terre.stadeDePlanete < 2)){
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
					glRotated(angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);	
			glLightiv(GL_LIGHT0,GL_POSITION,LightPos);
					glRotated(-angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);
		}
		else{
			if(Terre.temperature > 1800){
			glColor3f(1.0f,1.0f - ((Terre.temperature-1800.0f)/1200.0f),1.0f - ((Terre.temperature-1800.0f)/1200.0f));
			}
			else{ glColor3f(1.0f,1.0f,1.0f);}
		}
			
		//Affichage Lignes ou Plein
		if(Terre.hollow)
			gluQuadricDrawStyle(params,GLU_LINE);	
		else
			gluQuadricDrawStyle(params,GLU_FILL);	
	
		
		GradTextures(texture_previous, texture_terre, 2);
		
		if(Terre.temperature > 1800){
			glColor3f(1.0f,1.0f - ((Terre.temperature-1800.0f)/1200.0f),1.0f - ((Terre.temperature-1800.0f)/1200.0f));
		}
		else{ glColor3f(1.0f,1.0f,1.0f);}
	

			//Rotation de la planete
			glRotated(angle_camera*VITESSE_ROTATION_CAMERA,0,0,1);
			
		
		//Dessin de la sphere : couche solide
		gluSphere(params,rayon,prec,prec);
		
			//Antirotation (+1-1 =0)
			glRotated(-angle_camera*VITESSE_ROTATION_CAMERA,0,0,1);
		
		EndGrad();
	//Couche atmospherique	
	
		if(Terre.humidite > POURCENT_HUMIDITE_POUR_NUAGES && Terre.temperature > TEMP_MIN_NUAGES && Terre.temperature < TEMP_MAX_NUAGES)
		 // L'apparition des nuages est liée au taux d'humidité, ainsi qu'a la temperature : l'eau ne doit pas etre gelee ou completement evaporée
		{
			//printf("Les conditions sont reunies pour creer des nuages, contemplez ! \n");
			
			glBindTexture(GL_TEXTURE_2D,texture_clouds);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			
			
			//Rotation tres subtile des nuages
			glRotated(angle_camera*(VITESSE_ROTATION_CAMERA+CLOUD_SPEED),0,0,1);
				//Dessin de la sphere : couche nuageuse
				gluSphere(params,rayon+0.025,50,50);
			//Antirotation (+1-1 =0)
			glRotated(-angle_camera*(VITESSE_ROTATION_CAMERA+CLOUD_SPEED),0,0,1);
			
			glDisable(GL_BLEND);
		}
			
		gluDeleteQuadric(params);
		
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);

    glEnd();
    glPopMatrix();

}

void drawSky(void)
{
    glPushMatrix();
	
		glBindTexture(GL_TEXTURE_2D,texture_sky);
	
		
		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);
			gluQuadricDrawStyle(params,GLU_FILL);
			
	if(menu < 3){
		glRotated(-angle_camera*VITESSE_ROTATION_CAMERA/10*Terre.velocite/100,0,0,1);
	}
	else glRotated(-angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);
		
		//Dessin de la SkyBox
		gluSphere(params,500,15,15);
			
		gluDeleteQuadric(params);
		
	if(menu <3){
		glRotated(angle_camera*VITESSE_ROTATION_CAMERA/10*Terre.velocite/100,0,0,1);
	}
	else glRotated(angle_camera*VITESSE_ROTATION_CAMERA/2*Terre.velocite/100,0,0,1);

    glEnd();
    glPopMatrix();

}

void afficheString(void * font, char *text,int x,int y)
{
	glRasterPos2i(x, y);

	while( *text != '\0' )
	{
		glutBitmapCharacter( font, *text );
		++text;
	}
}

int ButtonInside(Button* b,int x,int y) 
{
	if(b) 
	{
	    if( x > b->x      
	    &&	x < b->x+b->w 
	    &&	y > b->y 
		&&	y < b->y+b->h )
			
			return 1;
	}

	return 0;
}

void relacheBtn(Button *b,int x,int y)
{
	if( ButtonInside(b,x,y) )
		{
			if(b->state != 1)
				b->state = 1;
			else
				b->state = 0;
			b->callbackFunction();
		}
}

void appuiBtn(Button *b,int x,int y)
{
	if(b)
	{
		if( ButtonInside(b,x,y) )
		{
			b->state = 1;
		}
	}
}

void passifBtn(Button *b,int x,int y)
{
	if(b)
	{
		//Souris sur le bouton
		if( ButtonInside(b,x,y) )
		{
			if( b->highlighted == 0 ) {
				//Changement etat
				b->highlighted = 1;
				//On force l'affichage
				glutPostRedisplay();
			}
		}
		else if( b->highlighted == 1 )
		//Si le bouton etait deja surligne
		{
			//Changement etat
			b->highlighted = 0;
			//On force l'affichage
			glutPostRedisplay();
		}
	}
}


void afficheBtn(Button *b)
{
	//Ces boutons ont une forme personnalisée style science-fiction
	int fontx;
	int fonty;
	float exp = 0;

	if(b)
	{
		//Changement de couleur au passage souris
		if (b->highlighted){
			glColor3f(0.453f,0.984f,0.875f);
			if(b->state) glColor3f(0,0.2f,0.4f);
			exp = (b->w)/10;
		}
		else if(b->state){
			//glColor3f(0.2028f,0.2f,0.251f);
			glColor3f(0,0.2f,0.4f);
			exp = 0;
		}
		else{
			//glColor3f(0.2028f,0.2f,0.251f);
			glColor3f(0.145f,0.07f,0.453f);
			exp = 0;
		}

		//	Background

		glBegin(GL_POLYGON);
			glVertex2i( b->x, b->y);
			glVertex2i( b->x, b->y+(b->h)*3/5);
			glVertex2i( b->x+(b->w)/8, b->y+b->h);
			if(b->state !=1) glColor3f(0.453f,0.984f,0.875f);
			else glColor3f(0.145f,0.07f,0.453f);
			glVertex2i(  exp + b->x+b->w, b->y+b->h );
			glVertex2i(  exp + b->x+b->w, b->y+(b->h)*3/5 );
			glVertex2i(  exp + b->x+b->w-(b->w)/8, b->y);
		glEnd();

		fontx = b->x + (b->w - glutBitmapLength(GLUT_BITMAP_HELVETICA_12,(const unsigned char*)b->label)) / 2 ;
		fonty = b->y + (b->h+10)/2;

		if (b->state) {
			fontx-=1;
			fonty-=1;
		}

		if(b->highlighted)
		{
			if(b->state)glColor3f(0,0,0);
			else glColor3f(1.0f,1.0f,1.0f);
			afficheString(GLUT_BITMAP_HELVETICA_12,b->label,fontx,fonty);
			fontx--;
			fonty--;
		}
		
			glColor3f(0.0f,0.0f,0.0f);
			if(b->state) glColor3f(1.0f,1.0f,1.0f);
		afficheString(GLUT_BITMAP_HELVETICA_12,b->label,fontx,fonty);
	}
}

//Affiche l'un des panneaux de l'ecran Chargement
void affichePanneauPlanete(int i, int nbmax){
			//Fond du cadre
			if(mouseIn(Souris, i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2))
				glColor3f(0.3028f,0.31f,0.361f);
			else
				glColor3f(0.2028f,0.2f,0.251f);
			rectanglePlein( i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2);
			
			//Icone planete
			glColor3f(0.4f,0.4f,0.4f);
			drawCercle(hF/(nbmax*2)+1, (i+0.5)*(lF/nbmax), (2*hF - hF/2)/5 -1, 30);
			glColor3f(0.1f,0.1f,0.1f);
			drawCercle(hF/(nbmax*2), (i+0.5)*(lF/nbmax), (2*hF - hF/2)/5, 30);
			
			if(mouseIn(Souris, i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2)){
				if(i%5 == 0)
					glColor3f(0.8f,0.46f,0.0f);
				else if(i%4 == 0)
					glColor3f(0.0f,0.43f,0.83f);
				else if(i%3 == 0)
					glColor3f(0.25f,0.0f,0.7f);
				else
					glColor3f(0.0f,0.56f,0.0f);
					
				drawDisque(hF/(nbmax*2)+1, (i+0.5)*(lF/nbmax)-1, (2*hF - hF/2)/5 -3, 30);
			}
			
			//Contour cadre
			glColor3f(0.4028f,0.41f,0.461f);
			glLineWidth(3);
			rectangleCreux( i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2);
			
			//Texte - Label
			if(mouseIn(Souris, i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2))
				glColor3f(0.95f,0.95f,0.95f);
			else
				glColor3f(0.5f,0.5f,0.5f);
			afficheString(GLUT_BITMAP_HELVETICA_18, liste[i] , (i*(lF/nbmax)) + glutBitmapLength(GLUT_BITMAP_HELVETICA_18,(const unsigned char*) liste[i])/2  , 4*hF/9 );
			
			if(mouseIn(Souris, i*(lF/nbmax), hF/5, (i+1)*(lF/nbmax+1), hF/2) && Souris.g == 1){
				strcpy(Terre.name,liste[i]);
				chargePlanete(&Terre);
				menu = 3;
			}
}


void drawMenu(void){
	//Affichage du menu principal

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,lF,hF,0,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRasterPos2i(10, 10);
	
	
	/* Viseur */
	/*
	glLineWidth(5);
	
	float x, y, w;
	x = lF/2;
	y = hF/2;
	w = lF/6;
	if( abs(hauteur) > 2 && abs(hauteur)< 20 ){
		w -= (abs(hauteur)*5);
	}
	else if( abs(hauteur) >= 20 ){
		w -= 100;
	}
	
	//Rond
	glColor3f(0.0f,0.0f,1.0f);
	drawCercle(w,x,y, 30);
	*/
	
	
	//Affichage des boutons sur l'interface
	afficheBtn(&NoyauBtn);
	afficheBtn(&EclairageBtn);
	
	if(menu == 6){ // On a appuyé sur Echap.
		//rectangle du fond
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor4f(0,0,0,0.75f);
		rectanglePlein(0, hF/4, lF, 3*hF/4);
		glDisable(GL_BLEND);

		
		//Texte
		glColor3f(0.8f,0.8f,0.8f);
		afficheString(GLUT_BITMAP_HELVETICA_18,"Sauvegarder / quitter ?",lF/2 - glutBitmapLength(GLUT_BITMAP_HELVETICA_18,(const unsigned char*) "Sauvegarder / quitter ?")/2, hF/3 );
		glColor3f(0.6f,0.6f,0.6f);
		afficheString(GLUT_BITMAP_HELVETICA_12,"Attention, si la planete existe le fichier portant son nom sera ecrase",lF/3, hF/3 +30 );
		
		int r = 10;
		glColor3f(0.1f,0.1f,0.1f);
		drawCercle(r, 3*lF/4 - 3*r, hF/4 + 3*r, 12);
		
		if(mouseIn(Souris, 3*lF/4 - 5*r, hF/4 + r, 3*lF/4 -r, hF/4 +5*r)){
			glColor3f(0.6f,0.0f,0.0f);
			if(Souris.g == 1)
				menu = 4;
		}
		else{
			glColor3f(0.8f,0.0f,0.0f);
		}
		drawDisque(r, 3*lF/4 - 3*r, hF/4 + 3*r, 12);
		
		afficheBtn(&sauverBtn);
		afficheBtn(&quitterBtn);
		
	}

}

void drawAccueil(void){
	//Affichage du menu principal

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,lF,hF,0,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRasterPos2i(10, 10);
	
	//AFFICHAGE FOND
	glBegin(GL_POLYGON);
	glColor3f(0.05f,0.05f,0.05f);
	glVertex2i(0,0);
	
	glVertex2i(0,hF);
	
	glColor3f(0.14f,0.07f,0.26f);
	glVertex2i(lF/2,hF);
	glEnd();
	
	if(menu == 0){
	//AFFICHAGE BOUTONS
	afficheBtn(&DemarrerBtn);
	afficheBtn(&ChargerBtn);
	//TEXTE
	glColor3f(0.8f,0.8f,0.8f);
	afficheString(GLUT_BITMAP_HELVETICA_18,"MY_C_PLANET",lF/12, 11*hF/18 );
	}
	
	if(menu == 1){	//Entree du nom de la planete
		int x= lF/4, y  = hF/3 +10, w= 2*lF/4, h= hF/9;
		
		glColor3f(0.0f,0.5f,0.8f);
		afficheString(GLUT_BITMAP_HELVETICA_18,"Nommez votre planete:",lF/4, hF/3 );
		
		glColor3f(0.05f,0.05f,0.05f);
		//Affichage de la zone de texte
		rectanglePlein(x,y,x+w,y+h);
		
		glLineWidth(2); 
		//Affichage contours
		
		glColor3f(0.8f,0.8f,0.8f);
		rectangleCreux(x, y, x+w, y+h);
		
		afficheString(GLUT_BITMAP_HELVETICA_18,Terre.name,x+10, y+ h/2 );
		afficheString(GLUT_BITMAP_HELVETICA_18,"|",x + glutBitmapLength(GLUT_BITMAP_HELVETICA_18,(const unsigned char*) Terre.name)+10, y+h/2 );
		
		afficheBtn(&ChargerBtn);
	}
	
	else if(menu == 2){	//Menu de chargement
		glColor3f(0.0f,0.5f,0.8f);
		afficheString(GLUT_BITMAP_HELVETICA_18,"Liste des planetes disponibles:",lF/4, hF/8 );
		
		glColor3f(0.95f,0.95f,0.95f);
		
		rectanglePlein(0,hF/8 + 20, lF, hF/8 +21);

	//Listing des planetes en sauvegarde reparties sur l'ecran
		for(int i=0; i < nbPlanetesSaved ; i++){
			affichePanneauPlanete(i,nbPlanetesSaved);
		}
		afficheBtn(&ChargerBtn);

	}
	glColor3f(0.6f,0.6f,0.6f);
	afficheString(GLUT_BITMAP_HELVETICA_12,"Thibaud SIMON - Projet ISEN DEV [2019]",1.5*lF/10, 7*hF/8 );

}

//Affichage de tout le contenu à dessiner
void DrawGL(void)
{

	glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluPerspective(70,(double)lF/hF,1,1000);

    glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

    glMatrixMode( GL_MODELVIEW );

    glLoadIdentity( );
	gluLookAt(rayon*(3-zoom),(4-zoom/2),hauteur,0,0,0,0,0,1);
	glColor3f(1,1,1);
	
	//Rotation : (degres_souhaités, sur x ?, sur y?, sur z?)
	glRotatef(rotatex,0.0,0.0,1.0);
	glRotatef(rotatey,0.0,1.0,0.0);

	drawSky();
	
		glClear(GL_ACCUM_BUFFER_BIT);

	
	if(menu > 3){
		
		drawSphere(rayon,prec);
		if(xaa > 0){
			drawPoussiere();
		}
		else{
			drawSphere(rayon,prec);	
		}
		if(Terre.stadeDeVie == 7) drawSatellite(nbsat);
	}
	else{
		fondMenu();
	}
			
	glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);

	if(intro == -1)
		afficheStats(Terre);
    


}
