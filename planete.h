#include <SDL/SDL.h>
#include <GL/gl.h>

//Proprietes graphiques OpenGL
#define FPS 60
#define LARGEUR_FENETRE 1024
#define HAUTEUR_FENETRE 768
#define VITESSE_ROTATION_CAMERA 0.03
#define POURCENT_HUMIDITE_POUR_NUAGES 50
#define TEMP_MIN_NUAGES 263	//Temperature MIN de -10°C
#define TEMP_MAX_NUAGES 333 //Temperature MAX de 60°C
#define CLOUD_SPEED 0.01
#define SAT_SPEED 3

#define PROBA_VIE 30

typedef struct planete{
	char name[51];
	float age;
	float masse;
	int temperature;
	float velocite;
	float humidite;
	int stadeDeVie;
	bool atmosphere;
	int stadeDePlanete;
	int generation;
	int hollow;
}planete;

typedef struct mouse{
	int x, y;	//Coordonnées d'abscisse et ordonnée
	int passif_x, passif_y; //Coordonnées passives
	int px, py;	//Previous coordonnées
	short g;	//Etat du bouton gauche
	short d;	//Etat du bouton droit
	short m;	//Etat du bouton milieu
	
	int mouvance; //En mouvement ?
}mouse;

typedef void (*ButtonCallback)();
typedef struct Button 
{
	int   x;							/* top left x coord of the button */
	int   y;							/* top left y coord of the button */
	int   w;							/* the width of the button */
	int   h;							/* the height of the button */
	int	  state;						/* the state, 1 if pressed, 0 otherwise */
	int	  highlighted;					/* is the mouse cursor over the control? */
	char* label;						/* the text label of the button */
	ButtonCallback callbackFunction;	/* A pointer to a function to call if the button is pressed */
}Button;

//Variables globales
double angle_camera;
double zoom;
float rotatex;
float rotatey;
double hauteur;

float rayon;
float prec;

int LightPos[4];
int MatSpec [4];
 
planete Terre;
float prevVel;
int intro;
int menu;
int eclairage;

int lF, hF;
char **liste; //Liste des planetes en sauvegarde
int nbPlanetesSaved;
mouse Souris;

float xaa;
int nbsat;

//Prototypes de fonctions
planete initPlanete(char* chaine_nom);
int sauvePlanete(planete p);
void chargePlanete(planete* p);

int aleatoireBorne(int a, int b);

void *age(void * arg);
void *launchIntro(void * arg);
void *gere_vivant(void * arg);

void gagnemasse(void);
void degazage(void);
void temperature(void);
void updateGeneration(int gen);

//Boutons
void showNoyau(void);
void startBtn(void);
void chargeBtn(void);
void showLights(void);
void saveBtn(void);
void quitBtn(void);

void passifBtn(Button *b, int x, int y);
void appuiBtn(Button *b, int x, int y);
void relacheBtn(Button *b, int x, int y);
void drawMenu(void);
void drawAccueil(void);
void DrawGL(void);
void afficheChaine(const char *chaine, float taille, float x, float y);
int listeFichiersDirectory(char * dossier, char*** liste, char* extension);

int mouseIn(mouse m, float x1, float y1, float x2, float y2);
void rectangleCreux(float x1, float y1, float x2, float y2);


//BOUTONS GLOBALS

Button NoyauBtn;
Button EclairageBtn;
Button ChargerBtn;
Button DemarrerBtn;
Button sauverBtn;
Button quitterBtn;

// Les identifiants de textures.
GLuint texture_noyau, texture_terre, texture_previous, texture_clouds;
GLuint texture_sky, texture_sky2, texture_caillous, texture_perf;

SDL_Surface * flipSurface(SDL_Surface * surface);
GLuint loadTexture(const char * filename, bool useMipMap);
