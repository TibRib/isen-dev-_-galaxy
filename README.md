ISEN DEV _ Galaxy : 
Projet par Thibaud SIMON réalisé d'octobre 2018 à janvier 2019.

Langage : C , GLSL
Librairies : SDL, OPENGL( GLU, GLUT)

Executable : programme_dyn

Attention ! Compilation: dynamique: requiert:
POUR UBUNTU / DEBIAN:
Telecharger les librarys SDL & SDL_image:
	sudo apt install libsdl2-dev libsdl-image1.2-dev

Telecharger libgl:
	sudo apt install libgl1-mesa-dev libglu1-mesa-dev

Telecharger lglut:
	sudo apt-get install freeglut3 freeglut3-dev


OPTIONELS:
Télecharger lX11:
	lX11:
	sudo apt-get install libx11-dev:i386 libx11-dev

Télécharger lpthread:
	pthread:
	sudo apt-get install libpthread-stubs0-dev


