#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#include "planete.h"

    static pthread_t tid;
	static int err;

void init(void);
void display(void);
void update(int g);
void changeSize(int w, int h);
void GestionClavierSpecial(int key, int x, int y);
void GestionClavier(unsigned char key, int x, int y);
void GereSouris(int button,int state,int x, int y);
void DeplacementSouris(int x, int y);
void DeplacementPassifSouris(int x, int y);
void updateFPS(void);

char fps_buffer[12];
int init_time, final_time, frame_count=0;

int main(int argc, char *argv[])
{
	
	srand(time(NULL));

    glutInit(& argc, argv );
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    
	//Proprietes definies pour la fenetre
    glutInitWindowSize(LARGEUR_FENETRE,HAUTEUR_FENETRE);
    glutInitWindowPosition(100,100);
    
    //Creation de la fenetre
        glutCreateWindow("My_C_planet");
    
    //Fonction d'initialisation
	init();
	
	//glutSetCursor(GLUT_CURSOR_INFO); //Passer le curseur en mode main
	//glutSetCursor(GLUT_CURSOR_RIGHT_ARROW); //Passer le curseur en mode pointeur classique
	
	//Fonction de callback temporisee (effectuee apres 1000 ms) appelée 1 fois
	glutTimerFunc(1000/FPS, update, 0);

	//Fonction de callbacks standards (valeurs)
		//glutIdleFunc(update);
	
	//Fonction des Inputs utilisateur
		//Souris (boutons)
		glutMouseFunc(GereSouris);
		
		//Souris (mouvement appuyé)
		glutMotionFunc(DeplacementSouris);
		
		//Souris  (mouvement passif)
		glutPassiveMotionFunc(DeplacementPassifSouris);
	
		//Clavier
		glutKeyboardFunc(GestionClavier);
		
		//Touches media / autres que des lettres:
		glutSpecialFunc(GestionClavierSpecial);
	
	//Fonction d'affichage
	glutDisplayFunc(display);
	
	//Ajustement matriciel de la taille
	glutReshapeFunc(changeSize);
	
	//Loop
	glutMainLoop();
    
    for(int a=0; a < 20; a++){
			free(liste[a]);
		}
	free(liste);


    return 0;
}

void resizeBtn(void){
		//Bouton de l'interface
	
	/* Montrer l'interieur */
	NoyauBtn.w = lF/6;
	NoyauBtn.h = hF/12;
	NoyauBtn.x = 8*lF/10;
	NoyauBtn.y = 8*hF/10;
	
	EclairageBtn.w = lF/6;
	EclairageBtn.h = hF/12;
	EclairageBtn.x = 8*lF/10;
	EclairageBtn.y = 9*hF/10;
	
	DemarrerBtn.w = lF/3;
	DemarrerBtn.h = hF/10;
	DemarrerBtn.x = lF/10 - lF/24;
	DemarrerBtn.y = 5*hF/8;
	
	ChargerBtn.w = lF/3;
	ChargerBtn.h = hF/10;
	ChargerBtn.x = lF/10;
	ChargerBtn.y = 6*hF/8;
	
	sauverBtn.w = lF/4;
	sauverBtn.h = hF/10;
	sauverBtn.x = lF/2 - lF/8 - lF/64;
	sauverBtn.y = 17 *hF/40;
	
	quitterBtn.w = lF/4 ;
	quitterBtn.h = hF/10;
	quitterBtn.x = lF/2 - lF/8 + lF/64;
	quitterBtn.y = 11*hF/20;
}

void initBtn(void){
	
	//Bouton de l'interface
	
	/* Montrer l'interieur */
	NoyauBtn.state =0;
	NoyauBtn.highlighted=0;
	NoyauBtn.label = "Montrer l'interieur";
	NoyauBtn.callbackFunction = showNoyau;
	
	/* Toggle eclairage */
	EclairageBtn.state =0;
	EclairageBtn.highlighted=0;
	EclairageBtn.label = "Eclairage : Actif";
	EclairageBtn.callbackFunction = showLights;
	
	/* Menu : Demarrer */
	DemarrerBtn.state =0;
	DemarrerBtn.highlighted=0;
	DemarrerBtn.label = "Demarrer";
	DemarrerBtn.callbackFunction = startBtn;
	
	/* Menu : charger */
	ChargerBtn.state =0;
	ChargerBtn.highlighted=0;
	ChargerBtn.label = "Charger";
	ChargerBtn.callbackFunction = chargeBtn;
	
	/* Echap : Sauver */
	sauverBtn.state =0;
	sauverBtn.highlighted=0;
	sauverBtn.label = "Sauver";
	sauverBtn.callbackFunction = saveBtn;
	
	/* Echap : Quitter */
	quitterBtn.state =0;
	quitterBtn.highlighted=0;
	quitterBtn.label = "Quitter";
	quitterBtn.callbackFunction = quitBtn;
	
	resizeBtn();
	

}

void init(void){
	
	lF = LARGEUR_FENETRE;
	hF = HAUTEUR_FENETRE;
	
	Souris.d = 0; Souris.g = 0; Souris.m = 0; Souris.mouvance = 0;
	
	initBtn();
	
	zoom = 0;
	rotatex = 0;
	rotatey = 0;
    
    //Init : OpenGL
	angle_camera = 0;
	hauteur = 3;

	rayon = 1;
	prec = 3;

	LightPos[0] = 20; LightPos[1] = 20; LightPos[2] = 40; LightPos[3] = 0;
	MatSpec[0] = 1; MatSpec[1] = 1; MatSpec[2] = 1; MatSpec[3] = 1; 
			
	Terre = initPlanete(""); //Initialisation de la planete sans nom
    texture_sky = loadTexture("img/sky/space/skyfurnish.jpg",false);
    texture_perf = loadTexture("img/life/png/1.png",true);
    texture_caillous = loadTexture("img/pierre/png/1.png",false);
    texture_noyau = loadTexture("img/magma/core.jpg",false);
    texture_previous = loadTexture("img/magma/core.jpg",false);
    texture_clouds = loadTexture("img/atmos/cloud_combined_2048.jpg",false);
    
    hauteur = 100;
    intro = 1;
    menu = 0;
    eclairage = 1;
    nbsat =0;
    
    xaa= -101;
    
    //Allocation de 20 sauvegardes
	liste = malloc (sizeof(char*) *20);
	
	for(int a =0; a < 20; a++){
			liste[a] = malloc(sizeof(char) * 100);
		}
	
	init_time = time(NULL);
		
	
 }
	
void display(void){
	//Fonction d'affichage
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	//RAFRAICHIS FENETRE
	
	if(menu < 3){
		DrawGL();
		drawAccueil();
	}
	else{
		DrawGL();
		if(intro == -1){
		drawMenu();

		/*Affichage IPS */
		glColor3f(0.7,0.8,0.9);
		glMatrixMode(GL_PROJECTION);
	    glPushMatrix();
	    glLoadIdentity();
	    gluOrtho2D(0.0, lF, 0.0,hF);
	    glMatrixMode(GL_MODELVIEW);
		afficheChaine(fps_buffer,12,lF*8.75,hF*2.2);
		glPopMatrix();
		}
	}


	glutSwapBuffers();
	updateFPS();
}

void update(int g){
	
	if(intro == 1 && menu == 3){
		updateGeneration(Terre.stadeDePlanete); //Charge une texture aleatoire de la planete
		//Creation du thread qui decompte la hauteur de la camera, pour l'intro.	
		 err = pthread_create(&tid, NULL, &launchIntro, NULL);
		if(err != 0)
			printf("\nErreur de la creation du thread intro\n");
		menu++;
	}
	
	//Initialisation après intro
	 if(intro == 0 && menu == 4){
		 //Passage à un etat inactif
		 intro = -1;
		 
		 //Creation du thread qui compte l'age
    		err = pthread_create(&tid, NULL, &age, NULL);
		if(err != 0)
			printf("\nErreur de la creation du thread age\n");
			
		//Creation du thread qui verifie l'etat d'accueil de la vie
    		err = pthread_create(&tid, NULL, &gere_vivant, NULL);
		if(err != 0)
			printf("\nErreur de la creation du thread vie \n");
			
		if(xaa == -101) xaa = 50;
		if(Terre.stadeDePlanete > 3) xaa = 0;
		

	}
	else if(menu == 4){
		//Fonction de traitement en boucle
		gagnemasse();
		degazage();
		temperature();
		
		prec = (int) Terre.masse*0.042;
		if(prec > 30) prec = 30;
		rayon = Terre.masse/300;

		if(Terre.velocite < 1) Terre.velocite = 1;
		if(Terre.humidite > 100) Terre.humidite = 100;
		
		if(Terre.temperature > 2000 && Terre.stadeDePlanete != 2) updateGeneration(2); //Fusion
		if(Terre.temperature > 800 && Terre.temperature < 2000 && Terre.stadeDePlanete != 3) updateGeneration(3); //Cooldown
		if(Terre.temperature < 800 && Terre.stadeDePlanete == 3 && xaa == -100) updateGeneration(4); //Planete tellurique
		if(Terre.stadeDePlanete ==4 && Terre.humidite > 50) updateGeneration(5); // Exoplanete
		
		if(xaa > -1) xaa -= VITESSE_ROTATION_CAMERA*Terre.velocite/3000;
		
		if(xaa <= 0 && xaa > -90){
			 xaa= -100;
			 //updateGeneration(2);
		}
		
	}
	if(rotatex > 359 || rotatex < -359) rotatex=0;
	if(rotatey > 359 || rotatey < -359) rotatey=0;
	

	angle_camera++;
	glutPostRedisplay();
	glutTimerFunc(1000/FPS, update, 0);

}

void updateFPS(void){
	frame_count++;
	final_time = time(NULL);
	if(final_time - init_time > 1)
	{
		sprintf(fps_buffer,"Im/s : %d\n",frame_count/(final_time-init_time));
		frame_count =0;
		init_time = final_time;
	}
}

void GereSouris(int button,int state,int x, int y){
	if (state == GLUT_DOWN) 
	{ //Bouton pressé
		switch(button) 
		{
		case GLUT_LEFT_BUTTON:
			Souris.g = 1;
			break;
		case GLUT_MIDDLE_BUTTON:
			Souris.m = 1;
			break;
		case GLUT_RIGHT_BUTTON:
			Souris.d = 1;
			break;
			
			Souris.x = x;
			Souris.y = y;
		}
	}
	else 
	{ //Bouton relaché
		switch(button) 
		{
		case GLUT_LEFT_BUTTON:
			Souris.g = 0;
					if(menu < 3){
					relacheBtn(&ChargerBtn,x,y);
					relacheBtn(&DemarrerBtn,x,y);
					}
					else if(menu < 6){
					relacheBtn(&NoyauBtn,x,y);
					relacheBtn(&EclairageBtn,x,y);
					}
					else{
					relacheBtn(&sauverBtn,x,y);
					relacheBtn(&quitterBtn,x,y);
					}
					
			break;
		case GLUT_MIDDLE_BUTTON:
			Souris.m = 0;
			break;
		case GLUT_RIGHT_BUTTON:
			Souris.d = 0;
			break;
		}
		Souris.x = x;
		Souris.px = x;
		Souris.y = y;
		Souris.py = y;
	}
	
	//button 3 = scroll up
	//button 4 = scroll down
	//Conditions sur la molette souris
	if(button == 3){
		
		//Equation logarithmique du zoom en fonction du rayon : z = -1,033ln(r) + 3,1341
		//Comportement lissé par rapport à la courbe polynomiale qui avait tendance à remonter sur les hautes valeurs
		if(zoom < (-1.033*log(rayon) + 3.1341))
		zoom += 0.1;
	}
	if(button == 4){
		if(zoom > -3)
			zoom -= 0.1;
		}
}

void DeplacementSouris(int x, int y){
		if(intro < 0){
		rotatex -= (float) (lF/2 - x)/(lF/4);
		rotatey -= (float) (hF/2 - y)/(hF/4);
	}
		Souris.x = x;
		Souris.y = y;
		
}

void DeplacementPassifSouris(int x, int y){
	if(menu < 3){
	passifBtn(&ChargerBtn,x,y);
	passifBtn(&DemarrerBtn,x,y);
	}
	else if(menu < 6){
	passifBtn(&NoyauBtn,x,y);
	passifBtn(&EclairageBtn,x,y);
	}
	else{
	passifBtn(&sauverBtn,x,y);
	passifBtn(&quitterBtn,x,y);
	}
	
	
	Souris.passif_x = x;
	Souris.passif_y = y;
}

void GestionClavierSpecial(int key, int x, int y)
{	
	if(intro == -1){
		switch (key)
		{	
			case GLUT_KEY_LEFT :
				Terre.velocite+=5;
			break;
			case GLUT_KEY_UP :
				if(hauteur < 480)hauteur++;
			break;
			case GLUT_KEY_RIGHT : 
				Terre.velocite-=5;
			break;
			case GLUT_KEY_DOWN : 
				if(hauteur > -480)hauteur--;
			break;
		}	
	}
}	

void GestionClavier(unsigned char key, int x, int y){
		 
	
	switch(key){
		
		case 27: //ECHAP
			if(menu == 6)
				menu = 4;
				
			else if(menu == 4){
				menu = 6;
			}
			
			if(menu > 0 && menu < 3){
				initBtn();
				menu = 0;
			}
			else if(menu == 0){
				quitBtn();
			}
			break;
			
			
		case 'r': //RESET
		case 'R':
			rotatex = 0;
			rotatey = 0;
			break;
			
		/*Fonctions developpeur : pour demonstration ! */
		case 'v': //RESET
		case 'V':
			Terre.stadeDeVie++;
			break;
			
	}
	//Entree du nom de la planete
	if(menu == 1){
		if(key >= 48 && strlen(Terre.name)< 51){ //Caracteres ASCII clavier
			char buffer[2];
			sprintf(buffer,"%c",key);
			strcat(Terre.name,buffer);
		}
		if(key == 8 && strlen(Terre.name) >= 1 ){ //Touches retour (supprimer la derniere lettre)
			char buffer[strlen(Terre.name)];
			strcpy(buffer, "");
			char chbuff[2];
			char k;
			for(int i=0; i < strlen(Terre.name)-1; i++){
				k = Terre.name[i];
				sprintf(chbuff, "%c",k);
				strcat(buffer,chbuff);
			}
			strcpy(Terre.name,buffer);
		}
			
	}
		
}

void changeSize(int w, int h) {

	//Eviter les soucis liés à une division par 0
	if(h == 0)
		h = 1;
	float ratio = 800/600;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

        // Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w,h);

	// Set the correct perspective.
	gluPerspective(45,ratio,1,1000);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
	
	lF = w;
	hF = h;
	
	resizeBtn();
}
