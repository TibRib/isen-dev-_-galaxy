#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdbool.h>
#include <math.h>

SDL_Surface * flipSurface(SDL_Surface * surface)
{
    int current_line,pitch;
    SDL_Surface * fliped_surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
                                   surface->w,surface->h,
                                   surface->format->BitsPerPixel,
                                   surface->format->Rmask,
                                   surface->format->Gmask,
                                   surface->format->Bmask,
                                   surface->format->Amask);



    SDL_LockSurface(surface);
    SDL_LockSurface(fliped_surface);

    pitch = surface->pitch;
    for (current_line = 0; current_line < surface->h; current_line ++)
    {
        memcpy(&((unsigned char* )fliped_surface->pixels)[current_line*pitch],
               &((unsigned char* )surface->pixels)[(surface->h - 1  -
                                                    current_line)*pitch],
               pitch);
    }

    SDL_UnlockSurface(fliped_surface);
    SDL_UnlockSurface(surface);
    return fliped_surface;
}


GLuint loadTexture(const char * filename, bool useMipMap)
{
    GLuint glID;
    SDL_Surface * picture_surface = NULL;
    SDL_Surface *gl_surface = NULL;
    SDL_Surface * gl_fliped_surface = NULL;
    Uint32 rmask, gmask, bmask, amask;

    picture_surface = IMG_Load(filename);
    if (picture_surface == NULL)
        return 0;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN

    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else

    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    SDL_PixelFormat format = *(picture_surface->format);
    format.BitsPerPixel = 32;
    format.BytesPerPixel = 4;
    format.Rmask = rmask;
    format.Gmask = gmask;
    format.Bmask = bmask;
    format.Amask = amask;

    gl_surface = SDL_ConvertSurface(picture_surface,&format,SDL_SWSURFACE);

    gl_fliped_surface = flipSurface(gl_surface);

    glGenTextures(1, &glID);

    glBindTexture(GL_TEXTURE_2D, glID);


    if (useMipMap)
    {

        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, gl_fliped_surface->w,
                          gl_fliped_surface->h, GL_RGBA,GL_UNSIGNED_BYTE,
                          gl_fliped_surface->pixels);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);

    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, 4, gl_fliped_surface->w,
                     gl_fliped_surface->h, 0, GL_RGBA,GL_UNSIGNED_BYTE,
                     gl_fliped_surface->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);


    SDL_FreeSurface(gl_fliped_surface);
    SDL_FreeSurface(gl_surface);
    SDL_FreeSurface(picture_surface);

    return glID;
}

#define FPS 50
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 600

void DrawGL();

#define VITESSE_ROTATION_CAMERA 0.01
#define VITESSE_ROTATION_PYRAMIDE 0.1
#define VITESSE_ROTATION_CUBE 0.05
double angle_camera = 0;
double angle_pyramide = 0;
double angle_cube = 180;
double x_cube = 2;
double hauteur = 3;

float rayon = 1;
float vit =  1;

int LightPos[4] = {20,20,40,0};
int MatSpec [4] = {1,1,1,1};

// Les identifiants de texture.
GLuint texture_noyau, texture_terre;
GLuint texture_sky, texture_sky2;

int main(int argc, char *argv[])
{
    SDL_Surface *ecran = NULL;
    SDL_Event event;
    const Uint32 time_per_frame = 1000/FPS;

    Uint32 last_time,current_time,elapsed_time; //for time animation
    Uint32 start_time,stop_time; //for frame limit

    SDL_Init(SDL_INIT_VIDEO);
    atexit(SDL_Quit);

    SDL_WM_SetCaption("SDL GL Application", NULL);
    ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 32, SDL_OPENGL);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluPerspective(70,(double)LARGEUR_FENETRE/HAUTEUR_FENETRE,1,1000);

    glEnable(GL_DEPTH_TEST);
    	glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    texture_sky = loadTexture("skyfurnish.jpg",false);
    texture_noyau = loadTexture("core.jpg",false);
    texture_terre = loadTexture("alba.jpg",false);

    last_time = SDL_GetTicks();
    for (;;)
    {

        start_time = SDL_GetTicks();


        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                exit(0);
                break;
			}
			
			switch (event.key.keysym.sym)
				{
					case SDLK_UP:
					hauteur++;
					break;
					
					case SDLK_DOWN:
					hauteur--;
					break;
					
					case SDLK_LEFT:
					vit += 1;
					break;
					
					case SDLK_RIGHT:
					vit -= 1;
					break;
					
				}

        }

        current_time = SDL_GetTicks();
        elapsed_time = current_time - last_time;
        last_time = current_time;

        DrawGL();

        stop_time = SDL_GetTicks();
        if ((stop_time - last_time) < time_per_frame)
        {
           SDL_Delay(time_per_frame - (stop_time - last_time));
        }
		angle_camera++;

    }

    return 0;
}

void drawSphere(float rayon, int prec)
{
    glPushMatrix();


	glMaterialiv(GL_FRONT_AND_BACK,GL_SPECULAR,MatSpec);
	glMateriali(GL_FRONT_AND_BACK,GL_SHININESS,100);
	
		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);

		gluQuadricDrawStyle(params,GLU_FILL);
		
		glBindTexture(GL_TEXTURE_2D,texture_noyau);
			//Dessin de la sphere : noyau interne
			gluSphere(params,rayon*17/100,prec,prec);
			
		
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHTING);
			
		gluQuadricDrawStyle(params,GLU_FILL);		
		glBindTexture(GL_TEXTURE_2D,texture_terre);
			//Dessin de la sphere : couche solide
			gluSphere(params,rayon,prec,prec);
			
			
			
		gluDeleteQuadric(params);
		
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);

    glEnd();
    glPopMatrix();

}

void drawSky()
{
    glPushMatrix();
	
		glBindTexture(GL_TEXTURE_2D,texture_sky);
	
		
		GLUquadric* params = gluNewQuadric();
			gluQuadricTexture(params,GLU_TRUE);
			gluQuadricDrawStyle(params,GLU_FILL);
			//Dessin de la SkyBox
			gluSphere(params,100,15,15);
			
		gluDeleteQuadric(params);

    glEnd();
    glPopMatrix();

}

void DrawGL()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    gluLookAt(3,4,hauteur,0,0,0,0,0,1);
	
	   glRotated(angle_camera/50,0,0,1);
	   
	drawSky();
		
		glEnable(GL_LIGHTING);
    	glEnable(GL_LIGHT0);
	
		glLightiv(GL_LIGHT0,GL_POSITION,LightPos);
	
			glRotated(-angle_camera*vit/10,0,0,1);
		glRotated(angle_camera/20,0,0,1);
		
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
	
    drawSphere(2,30);
		

    glFlush();
    SDL_GL_SwapBuffers();
}
